def vowel_swapper(string):
    # ==============
    # Your code here
    string2 = string
    string2 = string2.replace('a','4')
    string2 = string2.replace('A','4')
    string2 = string2.replace('e','3')
    string2 = string2.replace('E','3')
    string2 = string2.replace('i','!')
    string2 = string2.replace('I','!')
    string2 = string2.replace('o','ooo')
    string2 = string2.replace('O','000')
    string2 = string2.replace('u','|_|')
    string2 = string2.replace('U','|_|')
    return string2
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console