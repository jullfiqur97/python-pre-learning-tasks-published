def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator=='+':
        result = int(a+b)
        result_str = []
        result_str.insert(0, str(result % 2))
        while result > 1:
            result = result//2
            result_str.insert(0,str(result%2))
        return "".join(result_str)
    elif operator=='-':
        result = int(a - b)
        result_str = []
        result_str.insert(0, str(result % 2))
        while result > 1:
            result = result // 2
            result_str.insert(0,str(result % 2))
        return "".join(result_str)
    elif operator=='*':
        result = int(a * b)
        result_str = []
        result_str.insert(0, str(result % 2))
        while result > 1:
            result = result // 2
            result_str.insert(0,str(result % 2))
        return "".join(result_str)
    elif operator=='/':
        result = int(a / b)
        result_str = []
        result_str.insert(0, str(result % 2))
        while result > 1:
            result = result // 2
            result_str.insert(0,str(result % 2))
        return "".join(result_str)
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console