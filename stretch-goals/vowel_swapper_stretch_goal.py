def vowel_swapper(string):
    # ==============
    # Your code here
    replacement = {'a':'4','A':'4','e':'3','E':'3','i':'!','I':'!','o':'ooo','O':'000','u':'|_|','U':'|_|'}
    splitted_string = string.split(' ')
    found = {'a':0,'e':0,'i':0,'o':0,'u':0}
    for each in range(len(splitted_string)):
        for char in range(len(splitted_string[each])):
            if splitted_string[each][char].lower() in found.keys() and found[splitted_string[each][char].lower()]==1:
                found[splitted_string[each][char].lower()] = -1
                list_char = list(splitted_string[each])
                list_char[char] = replacement[splitted_string[each][char]]
                splitted_string[each] = "".join(list_char)


            elif splitted_string[each][char].lower() in found.keys() and found[splitted_string[each][char].lower()]==0:
                found[splitted_string[each][char].lower()] = 1

    string = " ".join(splitted_string)
    return string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
